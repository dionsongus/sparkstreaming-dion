/**
  * Created by dion on 3/26/18.
  */

package dion.test

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kudu.spark.kudu.KuduContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.monotonicallyIncreasingId
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe

case class Yellow(vendorid: String, tpep_pickup_datetime: String, tpep_dropoff_datetime: String, passenger_count: Int, trip_distance: Float, pickup_longitude: Float, pickup_latitude: Float, ratecodeid: String, store_and_fwd_flag: String, dropoff_longitude: Float, dropoff_latitude: Float, payment_type: String, fare_amount: Float, extra: Float, mta_tax: Float, tip_amount: Float, tolls_amount: Float, improvement_surcharge: Float, total_amount: Float)
//defining the following companion object makes the code a little easier to read. But it doesn't replace the case class definition.
//Why is it an object rather than a function? Some thought an object may not need the case class definition but apparently that's not true.
object Yellow {
  def create (csvString: String): Yellow = {
    def values = csvString.split(",")
    Yellow(values(0),
      values(1),
      values(2),
      values(3).toInt,
      values(4).toFloat,
      values(5).toFloat,
      values(6).toFloat,
      values(7),
      values(8),
      values(9).toFloat,
      values(10).toFloat,
      values(11),
      values(12).toFloat,
      values(13).toFloat,
      values(14).toFloat,
      values(15).toFloat,
      values(16).toFloat,
      values(17).toFloat,
      values(18).toFloat)
  }
}

object viaDStream {
  //case class Yellow(VendorID: String, tpep_pickup_datetime: String, tpep_dropoff_datetime: String, passenger_count: Int, trip_distance: Float, pickup_longitude: Float, pickup_latitude: Float, RatecodeID: String, store_and_fwd_flag: String, dropoff_longitude: Float, dropoff_latitude: Float, payment_type: String, fare_amount: Float, extra: Float, mta_tax: Float, tip_amount: Float, tolls_amount: Float, improvement_surcharge: Float, total_amount: Float)

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[2]").setAppName("App")
    val ssc = new StreamingContext(conf, Seconds(5))

    //case class Yellow(VendorID: String, tpep_pickup_datetime: String, tpep_dropoff_datetime: String, passenger_count: Int, trip_distance: Float, pickup_longitude: Float, pickup_latitude: Float, RatecodeID: String, store_and_fwd_flag: String, dropoff_longitude: Float, dropoff_latitude: Float, payment_type: String, fare_amount: Float, extra: Float, mta_tax: Float, tip_amount: Float, tolls_amount: Float, improvement_surcharge: Float, total_amount: Float)


    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "master1.valhalla.phdata.io:9093,master2.valhalla.phdata.io:9093,master3.valhalla.phdata.io:9093",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "use_a_separate_group_id_for_each_stream", //consumer group needed for sparkstreamkafka but not in regular kafka.
      "auto.offset.reset" -> "earliest",
      //"partition" -> "0", "offset" -> "302",
      "enable.auto.commit" -> (false: java.lang.Boolean),
      "security.protocol" -> "SASL_SSL",
      "ssl.truststore.location" -> "/opt/cloudera/security/pki/jks/jssecacerts-java-1.8.jks",
      "ssl.truststore.password" -> "changeit",
      "sasl.kerberos.service.name" -> "kafka"
    )

    val topics = Array("dsong_test3")
    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )

    //stream.print()

    //Note the schema works for 2017-01 but not 2016-01

    val cleanStream = stream.map(_.value).filter(_.contains("2016-01"))

    println("----------------cleanStream------------------") //all the lines showed up in the begining
    /*    18/03/29 15:21:28 WARN kafka010.KafkaUtils: overriding receive.buffer.bytes to 65536 see KAFKA-3135
    ----------------cleanStream------------------
    ----------------cleanStream.split------------------
    ----------------cleanStream.split.castToYellow------------------
    18/03/29 15:21:29 INFO kafka010.DirectKafkaInputDStream: Slide time = 5000 ms*/
    cleanStream.print()
    //output: 2,2016-01-01 00:00:00,2016-01-01 00:00:00,2,1.10,-73.990371704101563,40.734695434570313,1,N,-73.981842041015625,40.732406616210937,2,7.5,0.5,0.5,0,0,0.3,8.8

    println("----------------cleanStream.split------------------")
    val foo: DStream[Array[String]] = cleanStream.map(_.split(","))
    foo.foreachRDD(x => println(x.take(5)))
    //output: [Ljava.lang.String;@2c9f5ad9
    //adding toString or use take(5) didn't help made not difference

    println("----------------cleanStream.split.castToYellow------------------")
    //val caseRDD: DStream[Yellow] = cleanStream.map(_.split(",")).map(x => Yellow(x(0).toString, x(1).toString, x(2).toString, x(3).toInt, x(4).toFloat, x(5).toFloat, x(6).toFloat, x(7).toString, x(8).toString, x(9).toFloat, x(10).toFloat, x(11).toString, x(12).toFloat, x(13).toFloat, x(14).toFloat, x(15).toFloat, x(16).toFloat, x(17).toFloat, x(18).toFloat))
    //caseRDD.print()
    //output: Yellow(2,2016-01-01 00:00:00,2016-01-01 00:00:00,2,1.1,-73.99037,40.734695,1,N,-73.98184,40.732407,2,7.5,0.5,0.5,0.0,0.0,0.3,8.8)
    //This seems to work

    //caseRDD.saveAsTextFiles("yellow2016-01_short")

    cleanStream.foreachRDD { rdd =>

      // Get the singleton instance of SparkSession
      val spark = SparkSession.builder.config(rdd.sparkContext.getConf).getOrCreate()
      import spark.implicits._

      //println(rdd)

      // Convert RDD[String] to DataFrame
      val df = rdd.map(s=> Yellow.create(s))
        //.map(_.split(",")) //same single column problem moving the split inside.
        //.map(x => Yellow(x(0).toString, x(1).toString, x(2).toString, x(3).toInt, x(4).toFloat, x(5).toFloat, x(6).toFloat, x(7).toString, x(8).toString, x(9).toFloat, x(10).toFloat, x(11).toString, x(12).toFloat, x(13).toFloat, x(14).toFloat, x(15).toFloat, x(16).toFloat, x(17).toFloat, x(18).toFloat))
        //won't compile with the above:value toDF is not a member of org.apache.spark.rdd.RDD[Yellow]. OK if moving Yellow declaration outside of main().
        //the casting to case class may be done either inside or outside.
        //.toDF("VendorID", "tpep_pickup_datetime", "tpep_dropoff_datetime", "passenger_count", "trip_distance", "pickup_longitude", "pickup_latitude", "RatecodeID", "store_and_fwd_flag", "dropoff_longitude", "dropoff_latitude", "payment_type", "fare_amount", "extra", "mta_tax", "tip_amount", "tolls_amount", "improvement_surcharge", "total_amount")
        //java.lang.IllegalArgumentException: requirement failed: The number of columns doesn't match.
        //Old column names (1): value
        //New column names (19): VendorID, tpep_pickup_datetime, tpep_dropoff_datetime, passenger_count, trip_distance, pickup_longitude, pickup_latitude, RatecodeID, store_and_fwd_flag, dropoff_longitude, dropoff_latitude, payment_type, fare_amount, extra, mta_tax, tip_amount, tolls_amount, improvement_surcharge, total_amount
        //at scala.Predef$.require(Predef.scala:224)
        .toDF()
      df.show()
      //val yellowDF = spark.createDataFrame(df)
      //yellowDF.show()

      /*    why is this single column?
            +--------------------+
            |               value|
            +--------------------+
            |2,2016-01-01 00:0...|
            |2,2016-01-01 00:0...|
              ...
            |1,2016-01-01 00:0...|
            |1,2016-01-01 00:0...|
            +--------------------+*/

      /* or this
+--------------------+
|               value|
+--------------------+
|[2, 2016-01-01 00...|
|[2, 2016-01-01 00...|
...
|[2, 2016-01-01 00...|
|[2, 2016-01-01 00...|
       */

      val master1 = "master1.valhalla.phdata.io:7051"
      val master2 = "master2.valhalla.phdata.io:7051"
      val master3 = "master3.valhalla.phdata.io:7051"
      val kuduMasters = Seq(master1, master2, master3).mkString(",")
      val kc = new KuduContext(kuduMasters, spark.sparkContext)

      var tablename = "impala::user_dsong.Yellow"
      val dfkey = df.withColumn("transid", monotonicallyIncreasingId)
      kc.upsertRows(dfkey, tablename)
    }

    /*    caseRDD.foreachRDD{ rdd =>
          val spark = SparkSession.builder.config(rdd.sparkContext.getConf).getOrCreate()
          import spark.implicits._

          println("-------------caseRDD.foreachRDD-------------") //this is output to the right place!
          //println(rdd) //output: MapPartitionsRDD[125] at map at viaDStream.scala:58
          //rdd.first() //causes error
          rdd.toDF.show()

        }*/

    // Start the computation
    ssc.start()
    ssc.awaitTermination()
  }
}