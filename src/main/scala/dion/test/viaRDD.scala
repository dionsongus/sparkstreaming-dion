/**
  * Created by dion on 3/26/18.
  */

package dion.test


import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kudu.spark.kudu.KuduContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.monotonicallyIncreasingId
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe

import scala.collection.JavaConverters._

object viaRDD {

  //case class Yellow(VendorID: String, tpep_pickup_datetime: String, tpep_dropoff_datetime: String, passenger_count: Int, trip_distance: Float, pickup_longitude: Float, pickup_latitude: Float, RatecodeID: String, store_and_fwd_flag: String, dropoff_longitude: Float, dropoff_latitude: Float, payment_type: String, fare_amount: Float, extra: Float, mta_tax: Float, tip_amount: Float, tolls_amount: Float, improvement_surcharge: Float, total_amount: Float)
  case class Yellow(vendorid: String, tpep_pickup_datetime: String, tpep_dropoff_datetime: String, passenger_count: Int, trip_distance: Float, pickup_longitude: Float, pickup_latitude: Float, ratecodeid: String, store_and_fwd_flag: String, dropoff_longitude: Float, dropoff_latitude: Float, payment_type: String, fare_amount: Float, extra: Float, mta_tax: Float, tip_amount: Float, tolls_amount: Float, improvement_surcharge: Float, total_amount: Float)

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[2]")
    val sc = new SparkContext(conf)
    val spark = SparkSession.builder.config(conf).getOrCreate()
    import spark.implicits._

    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "master1.valhalla.phdata.io:9093,master2.valhalla.phdata.io:9093,master3.valhalla.phdata.io:9093",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "viaRDD", //consumer group needed for sparkstreamkafka but not in regular kafka.
      "auto.offset.reset" -> "earliest",
      "enable.auto.commit" -> (false: java.lang.Boolean),
      "security.protocol" -> "SASL_SSL",
      "ssl.truststore.location" -> "/opt/cloudera/security/pki/jks/jssecacerts-java-1.8.jks",
      "ssl.truststore.password" -> "changeit",
      "sasl.kerberos.service.name" -> "kafka"
    )
    // topic, partition, inclusive starting offset, exclusive ending offset
    val offsetRanges = Array(OffsetRange("dsong_test3", 0, 324, 331))
    val rdd = KafkaUtils.createRDD[String, String](sc, kafkaParams.asJava, offsetRanges, PreferConsistent)
    val cleanRDD = rdd.map(record => record.value().split(","))
      .map(x => Yellow(x(0), x(1), x(2).toString, x(3).toInt, x(4).toFloat, x(5).toFloat, x(6).toFloat, x(7).toString, x(8).toString, x(9).toFloat, x(10).toFloat, x(11).toString, x(12).toFloat, x(13).toFloat, x(14).toFloat, x(15).toFloat, x(16).toFloat, x(17).toFloat, x(18).toFloat))
    val df = cleanRDD.toDF
    //println("----------------------------------------------------------------------------")
    //println(lines.take(2).toString) //nothing is printed
    //cleanRDD.saveAsTextFile("file:/home/dsong/kafka/viaRDDoutput")
    df.show()

    val master1 = "master1.valhalla.phdata.io:7051"
    val master2 = "master2.valhalla.phdata.io:7051"
    val master3 = "master3.valhalla.phdata.io:7051"
    val kuduMasters = Seq(master1, master2, master3).mkString(",")
    val kc = new KuduContext(kuduMasters, sc)

    var tablename = "impala::user_dsong.Yellow"
    val dfkey = df.withColumn("transid", monotonicallyIncreasingId)
    kc.upsertRows(dfkey, tablename)
  }
}