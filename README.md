This is a simple pipeline that reads a Kafka producer and ingests into an existing kudu table. 
The ingestion can be done either with the viaRDD class or the viaDStream class. 
The viaRDD class operates in batch mode. 
The viaDStream class is a streaming operation. 